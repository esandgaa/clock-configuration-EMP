SOURCES=main.cpp clk_configuration.cpp
OBJECTS=$(addprefix $(BUILD_DIR)/,$(SOURCES:.cpp=.o))
DEPS=$(addprefix $(BUILD_DIR)/,$(SOURCES:.cpp=.d))
#LIBS=-luio
EXE=clk_configuration
CXXFLAGS=-I. -std=c++11

# Default target = host
BUILD_DIR=build/host
BIN_DIR=bin/host

#Making for host
#Run: make target=host
ifeq (${target},host)
	CXX=g++
	BUILD_DIR=build/host
	BIN_DIR=bin/host
endif

# Making for zynq ultrascale
# Run: make target=zynqMP
ifeq (${target},zynqMP)
	CXX=aarch64-linux-gnu-g++
	BUILD_DIR=build/zynqMP
	BIN_DIR=bin/zynqMP
endif

# Making for zynq
# Run: make target=zynq
ifeq (${target},zynq)
	CXX=arm-linux-gnueabihf-g++
	BUILD_DIR=build/zynq
	BIN_DIR=bin/zynq
endif

all: $(BIN_DIR)/$(EXE)
# Linker
$(BIN_DIR)/$(EXE): $(DEPS) $(OBJECTS) # << Check the $(DEPS) new dependency
	mkdir -p $(BIN_DIR)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJECTS) #$(LIBS)

# Rule that describes how a .o (object) file is created from a .cpp file
$(BUILD_DIR)/%.o: %.cpp
	mkdir -p $(BUILD_DIR)
	$(CXX) -c $< -o $@ $(CXXFLAGS)

# Rule that describes how a .d (dependency) file is created from a .cpp file
$(BUILD_DIR)/%.d: %.cpp
	mkdir -p $(BUILD_DIR)
	$(CXX) -MT$@ -MM $(CXXFLAGS) $< > $@
	$(CXX) -MT$(@:.d=.o) -MM $(CXXFLAGS) $< >> $@

.PHONY: clean
clean:
	@rm -rf bin/ build/
	@echo "All cleaned up. :o)"

#Avoid making dependency files just to remove them again
ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif

