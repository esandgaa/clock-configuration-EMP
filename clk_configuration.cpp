/*
 * Clock configuration
 * This code is built upon the a code from Cosmin Tanislav.
 * It has been modified, and some extra functions has been implemented 
 * to suit this application. 
 * 
 * The original code can be found here: 
 * https://github.com/Digilent/linux-userspace-examples/blob/master/i2c_example_linux/src/i2c.c
 */

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "clk_configuration.hpp"
#include "EMP_silabs_Design_Report.h"

/*
 * Start the I2C device.
 *
 * @param dev points to the I2C device to be started, must have filename and addr populated
 *
 * @return - 0 if the starting procedure succeeded
 *         - negative if the starting procedure failed
 */
int i2c_start(struct I2cDevice* dev) {
	int fd;
	int rc;

	/*
	 * Open the given I2C bus filename.
	 */
	fd = open(dev->filename, O_RDWR);
	if (fd < 0) {
		rc = fd;
		goto fail_open;
	}

	/*
	 * Set the given I2C slave address.
	 */
	rc = ioctl(fd, I2C_SLAVE, dev->addr);
	if (rc < 0) {
		goto fail_set_i2c_slave;
	}

	dev->fd = fd;

	return 0;

fail_set_i2c_slave:
	close(fd);
fail_open:
	return rc;
	

}

/*
 * Read data from the I2C device.
 *
 * @param dev points to the I2C device to be read from
 * @param buf points to the start of buffer to be read into
 * @param buf_len length of the buffer to be read
 *
 * @return - number of bytes read if the read procedure succeeded
 *         - 0 if no bytes were read
 *         - negative if the read procedure failed
 */
int i2c_read(struct I2cDevice* dev, uint8_t *buf, size_t buf_len) {
	return read(dev->fd, buf, buf_len);
}

/*
 * Write data to the I2C device.
 *
 * @param dev points to the I2C device to be write to
 * @param buf points to the start of buffer to be written from
 * @param buf_len length of the buffer to be written

 * @return - number of bytes written if the write procedure succeeded
 *         - 0 if no bytes were written
 *         - negative if the read procedure failed
 */
int i2c_write(struct I2cDevice* dev, uint8_t *buf, size_t buf_len) {
	return write(dev->fd, buf, buf_len);
	
}

/*
 * Read data from a register of the I2C device.
 *
 * @param dev points to the I2C device to be read from
 * @param reg the register to read from
 * @param buf points to the start of buffer to be read into
 * @param buf_len length of the buffer to be read
 *
 * @return - number of bytes read if the read procedure succeeded
 *         - 0 if no bytes were read
 *         - negative if the read procedure failed
 */
int i2c_readn_reg(struct I2cDevice* dev, uint8_t reg, uint8_t *buf, size_t buf_len) {
	int rc;

	/*
	 * Write the I2C register address.
	 */
	rc = i2c_write(dev, &reg, 1);
	if (rc <= 0) {
		printf("%s: failed to write i2c register address\r\n", __func__);
		return rc;
	}

	/*
	 * Read the I2C register data.
	 */

	rc = i2c_read(dev, buf, buf_len);
	if (rc <= 0) {
		
		printf("%s: failed to read i2c register data\r\n", __func__);
		return rc;
	}

	return rc;
}

/*
 * Write data to the register of the I2C device.
 *
 * @param dev points to the I2C device to be written to
 * @param reg the register to write to
 * @param buf points to the start of buffer to be written from
 * @param buf_len length of the buffer to be written
 *
 * @return - number of bytes written if the write procedure succeeded
 *         - 0 if no bytes were written
 *         - negative if the write procedure failed
 */
int i2c_writen_reg(struct I2cDevice* dev, uint8_t reg, uint8_t *buf, size_t buf_len) {
	uint8_t *full_buf;
	int full_buf_len;
	int rc;
	int i;

	/*
	 * Allocate a buffer that also contains the register address as
	 * the first element.
	 */
	full_buf_len = buf_len + 1;
	full_buf = (uint8_t*) malloc(sizeof(uint8_t) * full_buf_len); //(uint8_t*)

	full_buf[0] = reg;
	for (i = 0; i < buf_len; i++) {
		full_buf[i + 1] = buf[i];
	}

	/*
	 * Write the I2C register address and data.
	 */
	rc = i2c_write(dev, full_buf, full_buf_len);
	if (rc <= 0) {
		printf("%s: failed to write i2c register address and data\r\n", __func__);
		goto fail_send;
	}

	free(full_buf);
	return 0;

fail_send:
	free(full_buf);
	return rc;
}

/*
 * Read value from a register of the I2C device.
 *
 * @param dev points to the I2C device to be read from
 * @param reg the register to read from
 *
 * @return - the value read from the register
 *         - 0 if the read procedure failed
 */
uint8_t i2c_read_reg(struct I2cDevice* dev, uint8_t reg) {
	uint8_t value = 0;
	i2c_readn_reg(dev, reg, &value, 1);
	return value;
}

/*
 * Write value to the register of the I2C device.
 *
 * @param dev points to the I2C device to be written to
 * @param reg the register to write to
 * @param value the value to write to the register
 *
 * @return - number of bytes written if the write procedure succeeded
 *         - 0 if no bytes were written
 *         - negative if the write procedure failed
 */
int i2c_write_reg(struct I2cDevice* dev, uint8_t reg, uint8_t value) {
	return i2c_writen_reg(dev, reg, &value, 1);
}

/*
 * Mask value of to a register of the I2C device.
 *
 * @param dev points to the I2C device to be written to
 * @param reg the register to write to
 * @param mask the mask to apply to the register
 *
 * @return - number of bytes written if the write procedure succeeded
 *         - 0 if no bytes were written
 *         - negative if the write procedure failed
 */
int i2c_mask_reg(struct I2cDevice* dev, uint8_t reg, uint8_t mask) {
	uint8_t value = 0;
	int rc;

	value = i2c_read_reg(dev, reg);
	value |= mask;

	rc = i2c_write_reg(dev, reg, value);
	if (rc <= 0) {
		return rc;
	}
	return 0;
}

/*
 * Stop the I2C device.
 *
 * @param dev points to the I2C device to be stopped
 */
void i2c_stop(struct I2cDevice* dev) {
	/*
	 * Close the I2C bus file descriptor.
	 */
	close(dev->fd);
}


/*
 * Sets visuals for output
 *
 * @param c character to be repeated
 * @param count numbers of times
 */
void repeat (char c , int count ) {
	for (int i = 0; i < count; i++) {
		printf("%c", c);
	}
	printf("\n");
}

void section(char headline[])
{
	const int LENGHT = 75;
	repeat('*', LENGHT);
	printf("\n\n %s\n", headline);
	repeat('*', LENGHT);
	printf("  Action:\tPage:\t  Register:\tValue:\t  Writing:\t Status:\n");
	repeat('*', LENGHT);
}



/*
 * Forging data from EMP_Silabs_design_Report.
 * It splits data into a matrix, where the first column has the value of
 * the page. Second has the register addr. The thrid has the value.
 *
 * @returns - A <n>x3 matrix where [ page, register, value ]
 */
int **extractData() {

	uint8_t bytes[2];
	uint16_t addr;

	/* Numbers of elements in array */
	size_t array_size = sizeof(si5345_revb_registers)/8;

	/* allocating rows */
	int **arr=(int **)malloc(sizeof(int *)*array_size);

	/* initialize matrix: data_set[row][column] */
	int data_set[array_size][3];
	int column = 0 ;

	/* Writes data to matrix from Silabs design report */
	for (int row = 0 ; row < array_size ; ++row) {

			/* allocating columns */
			arr[row] = (int *)malloc(sizeof(int)*array_size);

			/* splits 16 bit into two 8-bits */
			addr = si5345_revb_registers[row].address;
			bytes[0] = addr >> 8; // high byte
			bytes[1] = addr & 0x00FF; // high byte

			/* writes address data first 8-bit to page column */
			arr[row][column] = bytes[0];
			
			/* writes address data last 8-bit to reg. addr. column */
			column=1;
			arr[row][column] = bytes[1];

			/* writes address data to thrid column for value 0xD8 */ 
			column=2;
			arr[row][column] = si5345_revb_registers[row].value;
			column=0;
	}
	return arr;
}

size_t size_of_array(){
	size_t array_size = sizeof(si5345_revb_registers)/8;
	return array_size;
}


/*
 * Function which displays the data extracted from EMP_silabs_Design_Report.
 * @param data_to_show Which data to show
 */
void show_data(int** data_to_show){
	int row;
	/* Numbers of elements in array */
	size_t array_size = sizeof(si5345_revb_registers)/8;

	printf("\n Values fetched from the EMP_Silabs_Design_Report.h\n");
	repeat('*', 75);
	printf(" Page:\t  Register:\tValues:\n");
	repeat('*', 75);

	for (int row = 0 ; row < array_size ; ++row) {
		int column = 0;
	 	printf("   %X\t   0x%X\t\t 0x%X\n",data_to_show[row][column], data_to_show[row][column+1],data_to_show[row][column+2]);	
	}
}



