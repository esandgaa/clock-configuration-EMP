/* 
 * clk_configuration.cpp
 *
 * Author: Emil Hammer
 * Date: 11/03/2022
 */

#include <stdio.h>
#include <unistd.h>
#include "clk_configuration.hpp"

#include <string.h>
#include <errno.h>
#include <iostream>
#include <string>

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#include <fcntl.h>
#include <stdlib.h>

int main()
{
	int j = 0;
	int page = 0;
	int page_val = 0;

	int rx;

	int **data_from_emp_silabs;

	/* Fetching data from Silabs design report to an matrix */
	data_from_emp_silabs = extractData();

	/* Displays data in matrix */
	//show_data(data_from_emp_silabs);

	struct I2cDevice dev;

	/* Set the I2C bus filename and slave address,
	 * "/dev/i2c-13" works! I dont know why and not ex. "/dev/i2c-12"! */
	dev.filename = (char *)"/dev/i2c-13";

	/* On-module Quad programmable PLL clock generator Si5345 (TE0808) [U35] has slave address 0x69 */
	dev.addr = 0x69;

	i2c_start(&dev);

	/* Calculate size of array from EMP_Silabs */
	size_t array_size = size_of_array() - 1;

	for (int i = 0; i < array_size; ++i)
	{

		/* Check if device is ready between readings/writings.
		 * - This register is repeated on every page*/
		rx = i2c_read_reg(&dev, 0xFE);
		if (rx != 0x0F) {

			printf("[Failure] Device not ready! Value of Reg 0xFE is: 0x%X\n", rx);
			return -1;
		}

		if (i == 0)
		{
			char message[] = "Start configuration preamble";
			section(message);
		}

		/* Delay 1 sec (should be at least 300 msec)
		 * Delay is worst case time for device to complete any calibration that is running
		 * due to device state change previous to this script being processed. */
		if (i == 3) // The first 3 values fetched are the preamble sequence.
		{
			sleep(1);
			char message[] = "Start configuration registers";
			section(message);
		} 
		else if (i == array_size - 5) // The last 5 values fetched are the postable sequence.
		{
			char message[] = "Start configuration postamble";
			section(message);
		}

		/* Fetch data from matrix */
		uint8_t page = data_from_emp_silabs[i][0];
		uint8_t reg = data_from_emp_silabs[i][1];
		uint8_t value = data_from_emp_silabs[i][2];

		/* This switch case selects the page to be written/read to, and allows us to exceed
		 * the slave address 255. Each page gets selected via 'i2c_write_reg(&dev, 0x0<n>01, 0);'
		 * where 'n' is the page we want to select.
		 * Page 0 starts from slave adresss 0. Page 1 from 256. Page 2 from 512 and so on.
		 */
		switch (page)
		{
		case 0:
			i2c_write_reg(&dev, 0x01, 0x00);
			page_val = 0;
			break;
		case 1:
			i2c_write_reg(&dev, 0x01, 0x01);
			page_val = 1;
			break;
		case 2:
			i2c_write_reg(&dev, 0x01, 0x02);
			page_val = 2;
			break;
		case 3:
			i2c_write_reg(&dev, 0x01, 0x03);
			page_val = 3;
			break;
		case 4:
			i2c_write_reg(&dev, 0x01, 0x04);
			page_val = 4;
			break;
		case 5:
			i2c_write_reg(&dev, 0x01, 0x05);
			page_val = 5;
			break;
		case 6:
			i2c_write_reg(&dev, 0x01, 0x06);
			page_val = 6;
			break;
		case 7:
			i2c_write_reg(&dev, 0x01, 0x07);
			page_val = 7;
			break;
		case 8:
			i2c_write_reg(&dev, 0x01, 0x08);
			page_val = 8;
			break;
		case 9:
			i2c_write_reg(&dev, 0x01, 0x09);
			page_val = 9;
			break;
		case 10:
			i2c_write_reg(&dev, 0x01, 0x0A);
			page_val = 10;
			break;
		case 11:
			i2c_write_reg(&dev, 0x01, 0x0B);
			page_val = 11;
			break;
		default:
			printf("[Failure] page not found! \n");
			return -1;

		}

		rx = i2c_read_reg(&dev, reg);

		if (rx == value)
		{
			printf("[Overwrote]\t %X\t   0x%X\t\t 0x%X\t   0x%X\t\t", page_val, reg, rx, value);
			i2c_write_reg(&dev, reg, value);
			rx = i2c_read_reg(&dev, reg);
			if (rx == value)
			{
				printf("[Success]\n");
				j++;
			}
			else
			{
				printf("[Failed]\n");
			}
		}
		else
		{
			printf("  [Wrote]\t %X\t   0x%X\t\t 0x%X\t   0x%X\t\t", page_val, reg, rx, value);
			i2c_write_reg(&dev, reg, value);
			rx = i2c_read_reg(&dev, reg);
			if (rx == value)
			{
				printf("[Success]\n");
				j++;
			}
			else
			{
				printf("[Failed]\n");
			}
			
		}
	}

	printf("\n\nTotal registers written to is: %d\n", array_size);

	/* Because of some self clearing register, some registers will fail validation */
	if (j >= array_size-2) {
		printf("Successfully configured clock generator Si5345!\n");

	} else {
		printf("Clock might not be configured right.\n");
	}

	printf("\nProgram finished.\n");

	i2c_stop(&dev);
	return 0;

}
