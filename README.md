# I2C Clock configuration for the EMP
This program is meant to replace the Silabs ClockBuilder PRO Kit.

It's based on a C file of the Design Report for the 'EMP_silabs_clock.slabtimeproj'. The file contains all information about the registers and values to write. If another file is desired to be used instead, it can be done with small modifications in the code. The C file can be exported from the ClockBuilder PRO application.

When executed, the code displays the addresses of all the registers written to (consists of a page and an address). Further, it shows their current value before writing and the value we wish to 'write'. Each 'write' is validated by checking if the value at the register after writing matches the value desired.

The I2C functions present in the program are built upon a code from Cosmin Tanislav. The original code can be found in the references.

# Prerequisites
- Able to communicate with the EMP, either through USB or SSH.
- This folder is installed and placed on the EMP.


# Usage
1. On the EMP, build the folder with the Makefile. This is done by typing _make_ inside the folder.
2. Next, run the code bin/host/clk_configuration.
3. If everything goes well, the communication is now established with the EMCI, and the 'RDY LED' should emit on the EMCI.


# Used references
- Original code from Cosmin Tanislav: https://github.com/Digilent/linux-userspace-examples/blob/master/i2c_example_linux/src/i2c.c
- Trenz Electronic TEBF0808 Manual (Info + table of device slave addresses): https://wiki.trenz-electronic.de/display/PD/TEBF0808+TRM
- Datasheet for I2C Switch: https://www.ti.com/lit/ds/symlink/tca9548a.pdf?ts=1643262427715&ref_url
- Si5345 Clock manual (Used for register lookups): https://www.skyworksinc.com/-/media/Skyworks/SL/documents/public/reference-manuals/Si5345-44-42-D-RM.pdf
